from distutils.core import setup
setup(name='mmm',
        version='0.3.1',
        description='Move My Music',
        author='Julio Biason',
        author_email='slow@slowhome.org',
        url='http://slowhome.org/projects/mmm-move-my-music/',
        packages=['mmmlib'],
        scripts=['mmm'],
        license='GPL',
        )

