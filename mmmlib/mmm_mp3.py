# This file is part of MMM.
#
# MMM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# MMM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MMM; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA
#
# Copyright (C) 2005-2007 Julio Biason

import mmm_utils
import logging
import mp3id3

class NoSuchFrameError (Exception):
    pass

class mmm_mp3:
    '''Class: mp3

    MP3 tag abstraction class'''

    def __init__ (self, file = None):
        '''Function: __init__

        Class constructor

        Parameters:
        - file: mp3 file to be opened.

        Exceptions:
        - FileNotFoundException: file cannot be found'''

        self.data = None

        self.load (file)
        return

    def load (self, file = None):
        if file is None:
            raise NoFilenameError

        try:
            self.data = mp3id3.read_id3_information (file)
            logging.debug ('File ' + file + ' loaded')
            logging.debug ('Self.data = %s' % (self.data))
        except mp3id3.UnrecognizedId3VersionError, version:
            logging.error ('Can''t read ID3 ' + version + ' from ' + file)
            self.data = None
        except mp3id3.FileWithoutId3Error:
            logging.error ('File ' + file + ' don''t have ID3 information')
            self.data = None
        except Exception, msg:
            logging.error ('Error opening ' + file)
            logging.error (msg)
            self.data = None
        return

    def get_artist (self):
        if self.data is None:
            logging.error ('Data not loaded')
            return None

        try:
            logging.debug ('Loading artist...')
            artist = self.data ['ARTIST']
            logging.debug (u'Artist: ' + str(artist))
            return artist
        except Exception, msg:
            logging.debug (msg)
            return NoSuchFieldError

    def get_album (self):
        if self.data is None:
            logging.error ('Data not loaded')
            return None

        try:
            album_title = self.data ['ALBUM']
            return album_title
        except:
            return NoSuchFieldError

    def get_track_number (self):
        if self.data is None:
            logging.error ('Data not loaded')
            return None

        try:
            tracknumber = str (self.data ['TRACK'])
            return tracknumber
        except:
            return NoSuchFieldError

    def get_title (self):
        if self.data is None:
            logging.error ('Data not loaded')
            return None

        try:
            title = self.data ['TITLE']
            return title
        except:
            return NoSuchFieldError
