# This file is part of MMM.
#
# MMM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# MMM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MMM; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA
#
# Copyright (C) 2005-2007 Julio Biason

import mmm_utils
import ogg.vorbis
import logging

class mmm_ogg:
    '''Class: ogg

    Ogg Vorbis tag abstraction class'''

    def __init__ (self, file = None):
        '''Function: __init__

        Class constructor

        Parameters:
        - file: ogg file to be opened.

        Exceptions:
        FileNotFoundException: file cannot be found'''

        self.file = None
        self.load (file)
        return

    def load (self, file = None):
        if file is None:
            raise NoFilenameError
        try:
            logging.debug ('Opening ' + file)
            vorbis = ogg.vorbis.VorbisFile (file)
            self.file = vorbis.comment().as_dict()
        except:
            logging.debug ('Error opening ' + file)
            self.file = None
        return

    def get_artist (self):
        if self.file is None:
            logging.debug ('File not loaded')
            return None
        
        try:
            logging.debug ('Getting artist')
            logging.debug (self.file)
            logging.debug (self.file ['ARTIST'][0])
            return self.file ['ARTIST'][0]
        except:
            raise NoSuchFieldError

    def get_album (self):
        if self.file is None:
            return None

        try:
            return self.file['ALBUM'][0]
        except:
            raise NoSuchFieldError

    def get_track_number (self):
        if self.file is None:
            return None
        try:
            return str (self.file['TRACKNUMBER'][0])
        except:
            raise NoSuchFieldError

    def get_title (self):
        if self.file is None:
            return None
        try:
            return self.file['TITLE'][0]
        except:
            raise NoSuchFieldError
