# This file is part of MMM.
#
# MMM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# MMM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MMM; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA
#
# Copyright (C) 2005-2007 Julio Biason

import logging

class UnrecognizedId3VersionError (Exception):
    def __init__ (self, major, revision):
        self.major    = major
        self.revision = revision

    def __str__ (self):
        return '2.' + str (self.major) + '.' + str (self.revision)

class FileWithoutId3Error (Exception):
    pass

def get_id3v2_information (file):
    easier_translations = { \
        'TIT2': 'TITLE', \
        'TALB': 'ALBUM', \
        'TPE1': 'ARTIST', \
        'TP1' : 'ARTIST', \
        'TAL' : 'ALBUM', \
        'TT2' : 'TITLE' }

    major    = ord (file.read (1))
    revision = ord (file.read (1))

    logging.debug ('File %s, Version: 2.%s.%s' % (file, major, revision))

    if major > 4:
        logging.debug ('Version not recognized')
        raise UnrecognizedId3VersionError (major, revision)

    if major == 4 and revision > 0:
        # support only to 2.4.0 (2.2.x and below should be
        # supported)
        logging.debug ('Version not recognized')
        raise UnrecognizedId3VersionError (major, revision)

    flags = file.read (1)   # not used right now
    # read id3 information
    size = file.read (4)
    header_size = (ord (size [0]) * (2**21)) + \
            (ord (size [1]) * (2**14)) + \
            (ord (size[2]) * (2**7)) + \
            ord (size[3])

    if major >= 3:
        frame_name_size   = 4
        frame_size_size   = 4
        frame_header_size = 10
    else:
        frame_name_size   = 3
        frame_size_size   = 3
        frame_header_size = 6

    if major >= 4:
        frame_size_matrix = [2**21, 2**14, 2**7, 1]
    else:
        frame_size_matrix = [2**24, 2**16, 2**8, 1]

    # frame
    result = {}
    while header_size > 0:
        frame_name  = file.read (frame_name_size)
        logging.debug ('Frame name: %s' % (frame_name))
        if frame_name == "":
            # padding, nothing more should follow
            break

        frame_size  = file.read (frame_size_size)

        if major >= 3:
            frame_flags = file.read (2)
        else:
            frame_size = '\0' + frame_size   # padding to keep the same formular for
                                            # both versions

        real_frame_size = 0
        for pos in range(4):
            real_frame_size += ord (frame_size [pos]) * frame_size_matrix [pos]

        if real_frame_size <= 0:
            # padding, nothing more should follow
            break

        header_size -= (real_frame_size + frame_header_size)
        text_frame = False
        if frame_name [0] == 'T':   # text frame
            text_frame = True
            encoding = ord (file.read (1))
            real_frame_size -= 1

            if encoding == 1:
                decoder = 'utf16'
            elif encoding == 2:
                decoder = 'utf16'
            elif encoding == 3:
                decoder = 'utf8'
            else:
                decoder = 'iso-8859-1'

        frame_contents = file.read (real_frame_size)
        if text_frame:
            frame_contents = unicode (frame_contents, decoder)

        frame_contents = frame_contents.strip ('\0').strip (' ')
        result [frame_name] = frame_contents

        if easier_translations.has_key (frame_name):
            result [easier_translations [frame_name]] = frame_contents
            logging.debug ('%s (%s) = %s' % (easier_translations [frame_name], frame_name, frame_contents))

        if frame_name == 'TRCK' or frame_name == 'TRK':
            # for easier access to track number, we'll add a
            # 'TRACK' value with just the track number, not
            # track number/total tracks
            split = frame_contents.find ('/')
            if split > -1:
                frame_contents = frame_contents [:split]

            result ['TRACK'] = int (frame_contents)

    return result

def get_id3v1_information (file):
    song    = file.read (30)
    artist  = file.read (30)
    album   = file.read (30)
    year    = file.read (4)
    comment = file.read (28)
    filler  = file.read (1) # unused, declared to stop the comment
    track   = file.read (1)
    genre   = file.read (1)

    result = {'TITLE': song.strip ('\0').strip (' '), \
            'ALBUM': album.strip ('\0').strip (' '), \
            'ARTIST': artist.strip ('\0').strip (' '), \
            'TRACK': ord (track)}

    return result

def read_id3_information (file = None):
    if file == None:
        return None

    file = open (file, 'r')  # and exception is thrown upwards
    id = file.read (3)  # header for ID3 files
    if id == 'ID3':
        result = get_id3v2_information (file)
    else:
        # try to find ID3v1 information
        file.seek (-128, 2)
        id = file.read (3)
        if id == 'TAG':
            result = get_id3v1_information (file)
        else:
            file.close ()
            raise FileWithoutId3Error

    file.close ()

    return result

# BONUS! TESTING UNIT!
def main ():
    try:
        data = read_id3_information ('music.mp3')
    except FileWithoutId3Error:
        print 'File don''t have Id3 information'
        return
    except UnrecognizedId3VersionError, version:
        print 'Parser don''t understand ID3 version', version
        return

    print data ['ARTIST'], ':', data ['ALBUM'], ',', data ['TRACK'], '.', data ['TITLE']

if __name__ == '__main__':
    main()
